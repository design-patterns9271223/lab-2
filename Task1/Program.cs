public abstract class Subscription
{
    public decimal CostPerMonth { get; set; }
    public int MinDuration { get; set; }
    public List<string> IncludedChannels { get; set; }

    public Subscription()
    {
        IncludedChannels = new List<string>();
    }
}

public class DomesticSubscription : Subscription
{
    public DomesticSubscription()
    {
        CostPerMonth = 10m;
        MinDuration = 1;
        IncludedChannels.Add("Standard Channel 1");
        IncludedChannels.Add("Standard Channel 2");
    }
}

public class EducationalSubscription : Subscription
{
    public EducationalSubscription()
    {
        CostPerMonth = 5m;
        MinDuration = 6;
        IncludedChannels.Add("Learning Channel 1");
        IncludedChannels.Add("Learning Channel 2");
    }
}

public class PremiumSubscription : Subscription
{
    public PremiumSubscription()
    {
        CostPerMonth = 20m;
        MinDuration = 1;
        IncludedChannels.Add("Exclusive Channel 1");
        IncludedChannels.Add("Exclusive Channel 2");
        IncludedChannels.Add("Exclusive Channel 3");
    }
}

public class SubscriptionFactory
{
    public static Subscription CreateSubscription(Type subType)
    {
        switch (subType.Name)
        {
            case nameof(DomesticSubscription):
                return new DomesticSubscription();
            case nameof(EducationalSubscription):
                return new EducationalSubscription();
            case nameof(PremiumSubscription):
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Subscription type is not recognized");
        }
    }
}

public interface ISubscriptionService
{
    Subscription CreateSubscription(Type subType);
}

public class WebPortal : ISubscriptionService
{
    public Subscription CreateSubscription(Type subType)
    {
        return SubscriptionFactory.CreateSubscription(subType);
    }
}

public class MobilePlatform : ISubscriptionService
{
    public Subscription CreateSubscription(Type subType)
    {
        return SubscriptionFactory.CreateSubscription(subType);
    }
}

public class PhoneSupport : ISubscriptionService
{
    public Subscription CreateSubscription(Type subType)
    {
        return SubscriptionFactory.CreateSubscription(subType);
    }
}

public class Program
{
    public static void Main()
    {
        ISubscriptionService service = new WebPortal();
        Subscription domesticSub = service.CreateSubscription(typeof(DomesticSubscription));
        Console.WriteLine($"Generated {domesticSub.GetType().Name} with a monthly cost of {domesticSub.CostPerMonth} and channels: {string.Join(", ", domesticSub.IncludedChannels)}");

        service = new MobilePlatform();
        Subscription eduSub = service.CreateSubscription(typeof(EducationalSubscription));
        Console.WriteLine($"Generated {eduSub.GetType().Name} with a monthly cost of {eduSub.CostPerMonth} and channels: {string.Join(", ", eduSub.IncludedChannels)}");

        service = new PhoneSupport();
        Subscription premiumSub = service.CreateSubscription(typeof(PremiumSubscription));
        Console.WriteLine($"Generated {premiumSub.GetType().Name} with a monthly cost of {premiumSub.CostPerMonth} and channels: {string.Join(", ", premiumSub.IncludedChannels)}");
    }
}
