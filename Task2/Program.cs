using System;

public interface IDevice
{
    string Name { get; }
}

public class Laptop : IDevice
{
    public string Name { get; private set; }

    public Laptop(string brand)
    {
        Name = $"{brand} Ultrabook";
    }
}

public class Netbook : IDevice
{
    public string Name { get; private set; }

    public Netbook(string brand)
    {
        Name = $"{brand} MiniPC";
    }
}

public class EBook : IDevice
{
    public string Name { get; private set; }

    public EBook(string brand)
    {
        Name = $"{brand} Reader";
    }
}

public class Smartphone : IDevice
{
    public string Name { get; private set; }

    public Smartphone(string brand)
    {
        Name = $"{brand} MobileDevice";
    }
}

public abstract class DeviceFactory
{
    public abstract IDevice CreateLaptop();
    public abstract IDevice CreateNetbook();
    public abstract IDevice CreateEBook();
    public abstract IDevice CreateSmartphone();
}

public class IProneFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("Apple Iphone");
    public override IDevice CreateNetbook() => new Netbook("Apple Iphone");
    public override IDevice CreateEBook() => new EBook("Apple Iphone");
    public override IDevice CreateSmartphone() => new Smartphone("Apple Iphone");
}

public class KiomFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("Xiaomi");
    public override IDevice CreateNetbook() => new Netbook("Xiaomi");
    public override IDevice CreateEBook() => new EBook("Xiaomi");
    public override IDevice CreateSmartphone() => new Smartphone("Xiaomi");
}

public class BalaxyFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("HyperX");
    public override IDevice CreateNetbook() => new Netbook("HyperX");
    public override IDevice CreateEBook() => new EBook("HyperX");
    public override IDevice CreateSmartphone() => new Smartphone("HyperX");
}

public class Program
{
    public static void Main()
    {
        DeviceFactory factory = new IProneFactory();
        DisplayDeviceNames(factory);

        factory = new KiomFactory();
        DisplayDeviceNames(factory);

        factory = new BalaxyFactory();
        DisplayDeviceNames(factory);
    }

    private static void DisplayDeviceNames(DeviceFactory factory)
    {
        Console.WriteLine(factory.CreateLaptop().Name);
        Console.WriteLine(factory.CreateNetbook().Name);
        Console.WriteLine(factory.CreateEBook().Name);
        Console.WriteLine(factory.CreateSmartphone().Name);
    }
}
