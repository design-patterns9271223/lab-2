using System;
using System.Collections.Generic;

public class Virus : ICloneable
{
    public string Name { get; set; }
    public string Type { get; set; }
    public double Weight { get; set; }
    public int Age { get; set; }
    public List<Virus> Offspring { get; set; }

    public Virus(string name, string type, double weight, int age)
    {
        Name = name;
        Type = type;
        Weight = weight;
        Age = age;
        Offspring = new List<Virus>();
    }

    public object Clone()
    {
        var clone = (Virus)this.MemberwiseClone();
        clone.Offspring = new List<Virus>(this.Offspring.Count);
        foreach (Virus child in this.Offspring)
        {
            clone.Offspring.Add((Virus)child.Clone());
        }
        return clone;
    }
}

public class Program
{
    public static void Main()
    {
        var population = CreateVirusPopulation();

        var clonedPopulation = CloneVirusPopulation(population);

        DisplayPopulationInfo(clonedPopulation);
    }

    public static List<Virus> CreateVirusPopulation()
    {
        var population = new List<Virus>();

        var parent = new Virus("Parent", "ParentType", 1.0, 10);

        var child1 = new Virus("Child1", "ChildType", 0.5, 5);
        var grandChild1 = new Virus("GrandChild1", "GrandChildType", 0.25, 2);
        var child2 = new Virus("Child2", "ChildType", 0.5, 5);

        child1.Offspring.Add(grandChild1);
        parent.Offspring.Add(child1);
        parent.Offspring.Add(child2);

        population.Add(parent);

        return population;
    }

    public static List<Virus> CloneVirusPopulation(List<Virus> population)
    {
        var clonedPopulation = new List<Virus>();

        foreach (var virus in population)
        {
            clonedPopulation.Add((Virus)virus.Clone());
        }

        return clonedPopulation;
    }

    public static void DisplayPopulationInfo(List<Virus> population)
    {
        foreach (var virus in population)
        {
            Console.WriteLine($"Virus: {virus.Name}, Type: {virus.Type}, Weight: {virus.Weight}, Age: {virus.Age}, Offspring Count: {virus.Offspring.Count}");
        }
    }
}
